<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.aedev V0.3.19 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.13 -->
# tpl_namespace_root 0.3.14

[![GitLab develop](https://img.shields.io/gitlab/pipeline/aedev-group/aedev_tpl_namespace_root/develop?logo=python)](
    https://gitlab.com/aedev-group/aedev_tpl_namespace_root)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/aedev-group/aedev_tpl_namespace_root/release0.3.13?logo=python)](
    https://gitlab.com/aedev-group/aedev_tpl_namespace_root/-/tree/release0.3.13)
[![PyPIVersions](https://img.shields.io/pypi/v/aedev_tpl_namespace_root)](
    https://pypi.org/project/aedev-tpl-namespace-root/#history)

>aedev_tpl_namespace_root package 0.3.14.

[![Coverage](https://aedev-group.gitlab.io/aedev_tpl_namespace_root/coverage.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_namespace_root/coverage/index.html)
[![MyPyPrecision](https://aedev-group.gitlab.io/aedev_tpl_namespace_root/mypy.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_namespace_root/lineprecision.txt)
[![PyLintScore](https://aedev-group.gitlab.io/aedev_tpl_namespace_root/pylint.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_namespace_root/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/aedev_tpl_namespace_root)](
    https://gitlab.com/aedev-group/aedev_tpl_namespace_root/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/aedev_tpl_namespace_root)](
    https://gitlab.com/aedev-group/aedev_tpl_namespace_root/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/aedev_tpl_namespace_root)](
    https://gitlab.com/aedev-group/aedev_tpl_namespace_root/)
[![PyPIFormat](https://img.shields.io/pypi/format/aedev_tpl_namespace_root)](
    https://pypi.org/project/aedev-tpl-namespace-root/)
[![PyPILicense](https://img.shields.io/pypi/l/aedev_tpl_namespace_root)](
    https://gitlab.com/aedev-group/aedev_tpl_namespace_root/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/aedev_tpl_namespace_root)](
    https://libraries.io/pypi/aedev-tpl-namespace-root)
[![PyPIDownloads](https://img.shields.io/pypi/dm/aedev_tpl_namespace_root)](
    https://pypi.org/project/aedev-tpl-namespace-root/#files)


## installation


execute the following command to install the
aedev.tpl_namespace_root package
in the currently active virtual environment:
 
```shell script
pip install aedev-tpl-namespace-root
```

if you want to contribute to this portion then first fork
[the aedev_tpl_namespace_root repository at GitLab](
https://gitlab.com/aedev-group/aedev_tpl_namespace_root "aedev.tpl_namespace_root code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(aedev_tpl_namespace_root):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/aedev-group/aedev_tpl_namespace_root/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://aedev.readthedocs.io/en/latest/_autosummary/aedev.tpl_namespace_root.html
"aedev_tpl_namespace_root documentation").
